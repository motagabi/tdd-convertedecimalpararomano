package com.br.romano;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class RomanoTeste {
    @Test
    public void testarConverterEmRomanos(){
        String resultado = Romano.converterEmRomanos(1000);
        Assertions.assertEquals("M",resultado);
    }
}
