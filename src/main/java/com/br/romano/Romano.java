package com.br.romano;

public class Romano {
    private static int[] DECIMAIS =
            {1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};
    private static String[] ROMANOS =
            {"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"};
    private static String resultado;

    public static String converterEmRomanos(int decimal) {
        int i = 0;
        while (decimal > 0) {
            if (decimal >= DECIMAIS[i]) {
                resultado = ROMANOS[i];
                decimal -= DECIMAIS[i];
            } else {
                i++;
            }
        }
        return resultado;
    }
}
